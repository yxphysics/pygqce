import numpy


class QCBank:
    '''
    class store quantum computer and relative info
    to avoid duplicated operations.
    '''
    def __init__(self):
        self.qc = None
        self.vqe_experiments = {}
        self.vqe_calibrations = {}
        self.dm_experiments = {}
        self.dm_calibrations = {}
        try:
            self.xopt_list = numpy.loadtxt("xopts.dat").tolist()
        except:
            self.xopt_list = [0., 0.]

    def get_qc(self, name):
        if self.qc is None:
            from pyquil.api import get_qc
            self.qc = get_qc(name)
        return self.qc

    def save_xopts(self):
        numpy.savetxt("xopts.dat", self.xopt_list)


qc_bank = QCBank()
