# have to be first line
import numpy
from pygrisb.gsolver.qiskit.gs import gsolver_qiskit_uccsd
from pygrisb.gsolver.forest.bank import qc_bank
from pygrisb.gsolver.forest.translate import  \
        get_pauliterms_from_weightedpauliop
from pygrisb.gsolver.forest._ansatz import input_ansatz
from pygrisb.gsolver.forest.util import get_pauli_term_index
# to be provided in working directory
import qc_ansatz as qca


class gsolver_vqe_wfn(gsolver_qiskit_uccsd):
    '''Gutzwiller solver with quantum variational solver
    on wavefunction simulator.
    '''
    def solve(self, _h_qop, _dm_qops):
        # hamiltonian in pyquil as a list of pauli terms.
        h_op = get_pauliterms_from_weightedpauliop(_h_qop)

        from pygrisb.gsolver.forest.paramexpt import paramExperimentWFN
        vqe = paramExperimentWFN(
                hamiltonian=h_op,
                )
        # check exact energy
        print('Exact ground state energy:',vqe.get_exact_gs())

        # load initial state
        vqe.set_ref_state(qca.ref_states[self.imp])
        # print('initial circuit energy estimate: ', vqe.objective_function())

        # load ansatz
        ansatz_circuit = input_ansatz(self.imp)
        vqe.set_ansatz(ansatz_circuit)

        # optimize
        from noisyopt import minimizeSPSA
        bounds = [[-numpy.pi, numpy.pi]]
        print(f"x0: {qc_bank.xopt_list[-2]:.3f}, {qc_bank.xopt_list[-1]:.3f}")

        ret = minimizeSPSA(vqe.objective_function,
                bounds=bounds,
                x0=qc_bank.xopt_list[-2:-1],
                niter=qca.niter,
                paired=False)
        ret['dm_ops'] = []
        qc_bank.xopt_list[-1] = ret["x"][0]

        from pyquil.api import WavefunctionSimulator
        for _dm_qop in _dm_qops:
            dm_op = get_pauliterms_from_weightedpauliop(_dm_qop)
            res = WavefunctionSimulator().expectation(
                    vqe.ref_state + vqe.ansatz,
                    dm_op,
                    {'theta': ret["x"]},
                    ).real
            ret['dm_ops'].append(sum(res))

        print(f"energy evaluated {ret['nfev']} times.")
        print(f"optimal x = {ret['x'][0]:.4f}")
        self._emol = ret['fun']
        return ret['dm_ops']


class gsolver_vqe_qc(gsolver_qiskit_uccsd):
    '''Gutzwiller solver using ccsd quantum variational solver.
    Using native experiments in pyquil with calibration.
    Symmetrization reading has overhead.
    '''
    def solve(self, _h_qop, _dm_qops):
        qc = qc_bank.get_qc(qca.qc)
        cq = qca.cq

        # hamiltonian in pyquil as a list of pauli terms
        h_op = get_pauliterms_from_weightedpauliop(_h_qop, cq=cq)

        from pygrisb.gsolver.forest.paramexpt import paramExperimentQC
        vqe = paramExperimentQC(
                qc=qc,
                hamiltonian=h_op,
                custom_qubits=cq,
                shots=qca.shots,
                symmetrization=qca.symmetrization,
                calibration=qca.calibration,
                verbose=qca.verbose,
                )
        # check exact energy
        print('Exact ground state energy:',vqe.get_exact_gs())

        if f"imp_{self.imp}" in qc_bank.vqe_experiments:
            vqe.set_experiment(qc_bank.vqe_experiments[f"imp_{self.imp}"])
            vqe.set_calibrations(qc_bank.vqe_calibrations[f"imp_{self.imp}"])
            vqe.set_append_mode(append=False)
            vqe.set_term_map(qc_bank.vqe_experiments[ \
                    f"imp_{self.imp}_term_map"].copy())
            vqe.set_gterm_map(qc_bank.vqe_experiments[ \
                    f"imp_{self.imp}_gterm_map"])
            vqe.set_nterms_current(qc_bank.vqe_experiments[ \
                    f"imp_{self.imp}_nterms"])
            vqe.set_term_identity(qc_bank.vqe_experiments[ \
                    f"imp_{self.imp}_term_identity"])
        else:
            vqe.set_ref_state(qca.ref_states[self.imp])
            # load ansatz
            ansatz_circuit = input_ansatz(self.imp)
            vqe.set_ansatz(ansatz_circuit)
            vqe.compile_tomo_expts()
            qc_bank.vqe_experiments[f"imp_{self.imp}"] = vqe.experiment
            qc_bank.vqe_calibrations[f"imp_{self.imp}"] = vqe.calibrations
            qc_bank.vqe_experiments[ \
                    f"imp_{self.imp}_term_map"] = vqe._term_map.copy()
            qc_bank.vqe_experiments[ \
                    f"imp_{self.imp}_gterm_map"] = vqe._gterm_map.copy()
            qc_bank.vqe_experiments[ \
                    f"imp_{self.imp}_nterms"] = vqe._nterms_cur
            qc_bank.vqe_experiments[ \
                    f"imp_{self.imp}_term_identity"] = vqe._term_identity

        # optimize
        from noisyopt import minimizeSPSA
        print(f"x0: {qc_bank.xopt_list[-2]:.3f}, {qc_bank.xopt_list[-1]:.3f}")
        bounds = [[-numpy.pi, numpy.pi]]

        ret = minimizeSPSA(vqe.objective_function,
                bounds=bounds,
                x0=qc_bank.xopt_list[-2:-1],
                niter=qca.niter,
                paired=False)

        qc_bank.xopt_list[-1] = ret["x"][0]

        # additional terms in dm_ops
        # available terms with expectation values
        isum = max(vqe._term_map)
        terms_more = []
        dm_offsets = numpy.zeros(len(_dm_qops))
        dm_id_coefs = []
        for i, _dm_qop in enumerate(_dm_qops):
            dm_op = get_pauliterms_from_weightedpauliop(_dm_qop, cq=cq)
            id_coefs = []
            for term in dm_op:
                idx = get_pauli_term_index(term, vqe._nq, cq)
                if idx == 0:
                    dm_offsets[i] += term.coefficient.real
                    continue
                imap = vqe._term_map[idx]
                if imap < 0:
                    terms_more.append(term)
                    isum += 1
                    vqe._term_map[idx] = isum
                    imap = isum
                id_coefs.append([imap, term.coefficient.real])
            dm_id_coefs.append(id_coefs)

        vqe._term_map -= max(vqe._term_map) - len(terms_more) + 1
        if f"imp_{self.imp}" in qc_bank.dm_experiments:
            vqe.set_experiment(qc_bank.dm_experiments[f"imp_{self.imp}"])
            vqe.set_calibrations(qc_bank.dm_calibrations[f"imp_{self.imp}"])
            vqe.set_append_mode(append=True)
            vqe.set_gterm_map(qc_bank.dm_experiments[ \
                    f"imp_{self.imp}_gterm_map"])
            vqe.set_nterms_current(qc_bank.dm_experiments[ \
                    f"imp_{self.imp}_nterms"])
            vqe.set_term_identity(None)
        else:
            vqe.compile_tomo_expts(
                    input_list=[terms_more, None, vqe._term_map],
                    append=True)
            qc_bank.dm_experiments[f"imp_{self.imp}"] = vqe.experiment
            qc_bank.dm_calibrations[f"imp_{self.imp}"] = vqe.calibrations
            qc_bank.dm_experiments[ \
                    f"imp_{self.imp}_gterm_map"] = vqe._gterm_map
            qc_bank.dm_experiments[ \
                    f"imp_{self.imp}_nterms"] = vqe._nterms_cur
        vqe.run_experiments({"theta": ret["x"]})

        ret['dm_ops'] = []
        for offset, id_coefs in zip(dm_offsets, dm_id_coefs):
            ret['dm_ops'].append(offset)
            for idx, coef in id_coefs:
                ret['dm_ops'][-1] += coef*vqe.term_es[idx]

        print(f"energy = {ret['fun']:.6f}, evaluated {ret['nfev']} times.")
        print(f"optimal x = {ret['x'][0]:.4f}")
        self._emol = ret['fun']
        return ret['dm_ops']
