from pyquil.paulis import PauliSum, PauliTerm
from openfermion.ops import QubitOperator


def get_pauliterms_from_weightedpauliop(weightedpauliop,
        cq=None):
    if weightedpauliop.is_empty():
        raise ValueError("Operator is empty!")
    if cq is None:
        cq = range(weightedpauliop.num_qubits)
    pauli_terms = []
    for weight, pauli in weightedpauliop._paulis:
        for iq, p1 in enumerate(pauli.to_label()):
            if iq == 0:
                term = PauliTerm(p1, cq[iq], weight)
            else:
                term *= PauliTerm(p1, cq[iq])
        pauli_terms.append(term)
    return pauli_terms


def pyquilpauli_to_qubitop(pyquil_pauli):
    """
    Convert a pyQuil PauliSum to an OpenFermion QubitOperator

    :param pyquil_pauli: pyQuil PauliSum
    :type pyquil_pauli: PauliSum

    :returns: a QubitOperator representing the PauliSum or PauliTerm
    :rtype: QubitOperator
    """
    if not isinstance(pyquil_pauli, PauliSum):
        raise TypeError("pyquil_pauli must be a pyquil PauliSum object")

    transformed_term = QubitOperator()
    # iterate through the PauliTerms of PauliSum
    for pauli_term in pyquil_pauli.terms:
        transformed_term += QubitOperator(
            term=tuple(zip(pauli_term._ops.keys(), pauli_term._ops.values())),
            coefficient=pauli_term.coefficient)

    return transformed_term
