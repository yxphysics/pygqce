from pyquil.quil import Program
from pyquil.paulis import exponential_map, PauliTerm


def get_ansatz(pauli_terms, param_indices=None):
    '''
    get parametrized exponential ansatz.
    '''
    nterms = len(pauli_terms)
    if param_indices is None:
        param_indices = list(range(nterms))
    else:
        assert(len(param_indices) == nterms), (
                f"param_indices {len(param_indices)} "
                f"not match with pauli_terms {nterms}.")
    n_parameters = max(param_indices) + 1
    prog = Program()
    # define the parameters.
    memref = prog.declare('theta',
            memory_type='REAL',
            memory_size=n_parameters)
    # construct the gates
    for term, i in zip(pauli_terms, param_indices):
        prog += exponential_map(term)(memref[i])

    return prog


def input_ansatz(imp):
    import qc_ansatz as qca
    # relabel the iq indices according to cq
    if hasattr(qca, "ansatz_ops_sub"):
        terms = qca.ansatz_ops_sub
    else:
        terms = qca.ansatz_ops
    term_labels = [reorder_digits(label, qca.cq)
            for label in terms[imp]]
    pauli_terms = [PauliTerm.from_compact_str(label) for label in term_labels]
    ansatz = get_ansatz(pauli_terms, qca.ansatz_params[imp])
    return ansatz


def reorder_digits(term, cq):
    terms = term.split("*")
    term_new = terms[0] + "*"
    num = ""
    if terms[1] == "I":
        term_new = term
    else:
        for i, s in enumerate(terms[1]):
            if s.isdigit():
                num += s
            if (not s.isdigit() and num != "") or i + 1  == len(terms[1]):
                num = str(cq[int(num)])
            if not s.isdigit():
                term_new += num + s
                num = ""
            elif i + 1  == len(terms[1]):
                term_new += num
    return term_new


