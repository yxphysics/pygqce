from typing import List, Union
from pyquil.api import QuantumComputer, WavefunctionSimulator
from pyquil.quil import Program
from pyquil.gates import RESET
from pyquil.paulis import PauliSum, PauliTerm
from pyquil.operator_estimation import group_experiments, TensorProductState
from pyquil.experiment import Experiment, ExperimentSetting, \
        correct_experiment_result
from pyquil.experiment._symmetrization import SymmetrizationLevel
from pyquil.experiment._calibration import CalibrationMethod
from openfermion.transforms import get_sparse_operator
import numpy as np, scipy as sp
from pygrisb.gsolver.forest.translate import pyquilpauli_to_qubitop
from pygrisb.gsolver.forest.util import get_pauli_term_index, add_pairs_cz, \
        get_pauli_term_without_cq
from scipy import interpolate
from pygrisb.run.timing import timeit


class paramExperimentBase:
    '''
    parametrized experiment.
    '''

    def __init__(self,
            hamiltonian: List[PauliTerm],
            ref_state: Program=Program(),
            ansatz: Program=Program(),
            initial_packed_amps: Union[List[float], None]=None,
            verbose: bool = False,
            ):
        """
        Parametric experiment class.
        One instantiates this class based on an input Hamiltonian.

        To debug and during development, set verbose=True
        to print output details to the console.

        :param [list(PauliTerm)] hamiltonian:
                Hamiltonian which one would like to simulate
        :param bool verbose: set to True for verbose output to the console,
                for all methods in this class
        """

        if len(hamiltonian) > 0:
            self.pauli_list = hamiltonian
            self._nq = self.get_qubit_req()
        else:
            raise ValueError("empty hamiltonian.")
        # whether to print debugging data to console
        self.verbose = verbose
        self.initial_packed_amps = initial_packed_amps
        self.ref_state = ref_state
        self.ansatz = ansatz

        # times of function call
        self.it_num = 0

    def objective_function(self, amps=None):
        """
        This function returns the Hamiltonian expectation value
        over the final circuit output state.
        If argument packed_amps is given,
        the circuit will run with those parameters.
        Otherwise, the initial angles will be used.

        :param [list(), numpy.ndarray] amps: list of circuit angles
                to run the objective function over.

        :return: energy estimate
        :rtype: float
        """

        if amps is None:
            packed_amps = self.initial_packed_amps
        elif isinstance(amps, np.ndarray):
            packed_amps = amps.tolist()
        elif isinstance(amps, list):
            packed_amps = amps
        else:
            raise TypeError('Please supply the circuit parameters'
                    ' as a list or np.ndarray')
        if packed_amps is None:
            memory_map = None
        else:
            memory_map = {'theta': packed_amps}

        res = self.evaluate_expval(memory_map)
        if self.verbose:
            self.it_num += 1
            print('black-box function call #' + str(self.it_num))
            print('Energy estimate is now:  ' + str(res))
            print('at angles:               ', packed_amps)
        return res

    def get_exact_gs(self, pauli_list=None, mode="e"):
        """
        Calculate the exact groundstate energy of the loaded Hamiltonian

        :param PauliList pauli_list: (optional) Hamiltonian of which
                one would like to calculate the GS energy

        :return: groundstate energy
        :rtype: float
        """
        if pauli_list is None:
            pauli_list = self.pauli_list
        paulis = [get_pauli_term_without_cq(term.copy(), self._cq) \
                for term in pauli_list]
        h = get_sparse_operator(
                pyquilpauli_to_qubitop(PauliSum(paulis)))
        if h.shape[0] > 1024:
            # sparse matrix eigenvalue decomposition is less accurate,
            # but is necessary for larger matrices
            w, v = sp.sparse.linalg.eigsh(h, k=2, which="SA")
        else:
            w, v = np.linalg.eigh(h.todense())
        idx = np.argmin(w)
        w = w[idx]
        v = np.flip(v[:, idx].reshape(-1))
        Egs =w.real
        if mode == "e":
            return Egs
        elif mode == "v":
            return v
        else:
            return Egs, v

    def get_qubit_req(self):
        """
        This method computes the number of qubits required
        to represent the desired Hamiltonian:
        * assumes pauli_list has been loaded properly.

        :return: number of qubits required in the circuit,
                as set by the Hamiltonian terms' indices.
        :rtype: int
        """

        if self.pauli_list is None:
            raise ValueError('Error: pauli_list not loaded properly.'
                    ' re-initialize VQE object')

        q_list = []
        for term in self.pauli_list:
            for q, st in term.operations_as_set():
                if q not in q_list:
                    q_list.append(q)
        return len(q_list)

    def verbose_output(self, verbose: bool = True):
        """
        :param bool verbose: set verbose output to console.
        """
        self.verbose = verbose

    def get_circuit(self):
        """
        This method returns the reference state preparation circuit
        followed by the ansatz circuit.

        :return: return the PyQuil program which defines the circuit.
                Excludes tomography rotations.
        :rtype: Program
        """
        return Program(self.ref_state + self.ansatz)

    def set_ref_state(self, prog: Program = Program()):
        """
        :param Program() prog: set a custom reference state preparation
                circuit as a program. All angles must be parametric.
        """
        self.ref_state = Program(prog)

    def set_ansatz(self, prog: Program = Program()):
        """
        :param Program() prog: set a custom ansatz circuit as a program.
                All variational angles must be parametric !
        """
        self.ansatz = Program(prog)

    def get_ansatz_2qubit_gates(self, gate2="CNOT"):
        """
        get number of two-qubit gates in the ansatz circuit.
        """
        n = f"{self.ansatz}".count(gate2)
        return n

    def set_initial_angles(self, angles: List[float]):
        """
        User-specify the initial angles for the experiment.

        :param list() angles: list of angles for the circuit
        """
        self.initial_packed_amps = angles

    def save_program(self, filename):
        """this saves the preparation circuit as a quil program
        which can be parsed with pyquil.parser.parse

        :param str filename: saves the quil program to this filename.
        """
        prog = self.ref_state + self.ansatz
        text = prog.out()
        with open(filename, "w") as text_file:
            text_file.write(text)


class paramExperimentWFN(paramExperimentBase):
    '''
    parametrized experiment with wave function simulator
    '''
    def __init__(self,
            hamiltonian: List[PauliTerm],
            ref_state: Program=Program(),
            ansatz: Program=Program(),
            initial_packed_amps: Union[List[float], None]=None,
            verbose: bool = False,
            ):
        super().__init__(
                hamiltonian,
                ref_state=ref_state,
                ansatz=ansatz,
                initial_packed_amps=initial_packed_amps,
                verbose=verbose,
                )
        self.pauli_sum = PauliSum(self.pauli_list)
        self._cq = None

    def evaluate_expval(self, memory_map, terms=None):
        if terms is None:
            terms = self.pauli_sum
        res = WavefunctionSimulator().expectation(
                self.ref_state + self.ansatz,
                terms,
                memory_map=memory_map).real
        return res

    def get_wavefunction(self, memory_map):
        p = self.ref_state + self.ansatz
        wf = WavefunctionSimulator().wavefunction(p, memory_map=memory_map)
        return wf.amplitudes


class paramExperimentQC(paramExperimentBase):
    '''
    parametrized experiment with qvm or device.
    '''
    def __init__(self,
            qc: QuantumComputer,
            hamiltonian: List[PauliTerm],
            ref_state: Program=Program(),
            ansatz: Program=Program(),
            initial_packed_amps: Union[List[float], None]=None,
            shots: int = 2**10,
            active_reset: bool = True,
            custom_qubits = None,
            error_mitigation = None,
            symmetrization: int = SymmetrizationLevel.EXHAUSTIVE,
            calibration: int = CalibrationMethod.PLUS_EIGENSTATE,
            verbose: bool = False,
            ):
        """

        Parametric experiment class.
        One instantiates this class based on an input Hamiltonian.

        For some QuantumComputer objects,
        the qubit lattice is not numbered 0..N-1
        but has architecture-specific logical labels.
        These need to be manually read from the lattice topology
        and specified in the list custom_qubits.
        On the physical hardware QPU,
        actively resetting the qubits is supported
        to speed up the repetition time of circuit..

        To debug and during development, set verbose=True
        to print output details to the console.

        :param [QuantumComputer(),None] qc:  object
        :param [list(PauliTerm)] hamiltonian:
                Hamiltonian which one would like to simulate
        :param int shots: number of shots in the Tomography experiments
        :param bool active_reset:  whether or not to actively reset the qubits
        :param bool verbose: set to True for verbose output to the console,
                for all methods in this class
        :param list() custom_qubits: list of qubits, i.e. [7,0,1,2] ordering
                the qubit IDs as they appear on the QPU
                lattice of the QuantumComputer() object.

        """
        super().__init__(
                hamiltonian,
                ref_state=ref_state,
                ansatz=ansatz,
                initial_packed_amps=initial_packed_amps,
                verbose=verbose,
                )
        # abstract QC. can refer to a qvm or qpu.
        # QC architecture and available gates decide the compilation of the
        # programs!
        self.qc = qc
        self._symmetrization = symmetrization
        self._calibration = calibration

        # number of shots in a tomography experiment
        self.shots = shots

        # whether or not the qubits should be actively reset.
        # False will make the hardware wait for 3 coherence lengths
        # to go back to |0>
        self.active_reset = active_reset

        # list of grouped experiments
        self.experiment = None

        # error mitigation
        self.error_mitigation = error_mitigation

        # real QPU has a custom qubit labeling
        self._cq = custom_qubits

        # circuit number of czs
        self.n_cz = []

        # array storing term expactation values
        self.term_es = None

    def set_term_map(self, term_map):
        self._term_map = term_map

    def set_gterm_map(self, term_map):
        self._gterm_map = term_map

    def set_append_mode(self, append=False):
        self._append_mode = append

    def set_term_identity(self, term_identity):
        self._term_identity = term_identity

    def create_term_mapping(self):
        '''
        map a general term index to the position in pauli_list.
        '''
        # set inf to trigger error if not set.
        term_map = -np.ones([4**self._nq], dtype=np.int)
        term_identity = None
        for i, term in enumerate(self.pauli_list):
            idx = get_pauli_term_index(term, self._nq, cq=self._cq)
            term_map[idx] = i
            if idx == 0:
                # identity
                term_identity = i
        self.set_term_map(term_map)
        self.set_term_identity(term_identity)
        return term_map

    def set_grouped_term_mapping(self, grouped_terms, term_map):
        # one-to-one mapping between terms in grouped expts and pauli_list
        gterm_map = []
        for terms in grouped_terms:
            for term in terms:
                idx = get_pauli_term_index(term, self._nq, cq=self._cq)
                gterm_map.append(term_map[idx])
        self.set_gterm_map(np.asarray(gterm_map))

    @timeit
    def compile_tomo_expts(self, input_list=None, append=False):
        """
        This method compiles the tomography experiment circuits
        and prepares them for simulation.
        Every time the circuits are adjusted,
        re-compiling the tomography experiments
        is required to affect the outcome.
        """
        # Are these experiments appedned to the previous one
        self.set_append_mode(append=append)
        # use Forest's sorting algo from the Tomography suite
        # to group Pauli measurements together
        settings = []
        if input_list is None:
            pauli_list = self.pauli_list
            term_map = self.create_term_mapping()
        else:
            pauli_list, term_identity, term_map = input_list
            self.set_term_identity(term_identity)
        # current number of pauli terms in expt.
        self.set_nterms_current(len(pauli_list))
        for i, term in enumerate(pauli_list):
            # skip an identity operator,
            if len(term.operations_as_set()) > 0:
                # initial state and term pair.
                settings.append(ExperimentSetting(
                        TensorProductState(),
                        term))
        # group_experiments cannot be directly run
        # because there may be experiment with multiple settings.
        # so here we just use group_experiments to sort out the settings,
        # then reset the experiments with additional measurements.
        experiments = Experiment(settings, Program())
        suite = group_experiments(experiments)
        # we just need the grouped settings.
        grouped_pauil_terms = []
        for setting in suite:
            group = []
            for i, term in enumerate(setting):
                pauil_term = term.out_operator.copy()
                # Coefficients to be multiplied.
                pauil_term.coefficient = complex(1.)
                if i == 0:
                    group.append(pauil_term)
                elif len(pauil_term) > len(group[0]):
                    # longest one first
                    group.insert(0, pauil_term)
                else:
                    group.append(pauil_term)
            # make sure the longest pauil_term contains all the small
            # pauil_terms. Otherwise, we prepare a bigger one.
            bigger_pauli = group[0]
            if len(group) > 1:
                for term in group[1:]:
                    for iq, op in term.operations_as_set():
                        if op != bigger_pauli[iq]:
                            assert(bigger_pauli[iq] == "I"), \
                                    (f"{term} and {group[0]} not compatible!")
                            if bigger_pauli is group[0]:
                                bigger_pauli == group[0].copy()
                            bigger_pauli *= PauliTerm(op, iq)
                if bigger_pauli is not group[0]:
                    # print(f"new pauli_term generated: {bigger_pauli}")
                    group.insert(0, bigger_pauli)
            grouped_pauil_terms.append(group)

        self.set_grouped_term_mapping(grouped_pauil_terms, term_map)

        # group settings with additional_expectations.
        grouped_settings = []
        for pauil_terms in grouped_pauil_terms:
            additional = None
            if len(pauil_terms) > 1:
                additional = []
                for term in pauil_terms[1:]:
                    # strange convention, need qubits rather them terms
                    additional.append(term.get_qubits())
            grouped_settings.append(
                    ExperimentSetting(TensorProductState(),
                            pauil_terms[0],
                            additional_expectations=additional))

        print('number of tomography experiments: ', len(grouped_settings))
        print(f'symmetrization = {self._symmetrization}')
        print(f'calibration = {self._calibration}')

        # get the uccsd program
        prog = Program()
        prog += RESET()
        prog += self.ref_state + self.ansatz
        prog.wrap_in_numshots_loop(shots=self.shots)
        self.experiment = Experiment(
                grouped_settings,
                prog,
                symmetrization=self._symmetrization,
                calibration=self._calibration,
                )

        # calibration expreimental results.
        if self.experiment.calibration != CalibrationMethod.NONE:
            self.calibrations = self.qc.calibrate(self.experiment)
        else:
            self.calibrations = None
        if self.error_mitigation is not None:
            self.add_error_mitigation_experiments()

    def add_error_mitigation_experiments(self, nadd=1):
        self.experiment_adds = []
        for i in range(nadd):
            prog, n_cz = add_pairs_cz(self.experiment.program, i+1)
            if len(self.n_cz) == 0:
                self.n_cz.append(n_cz)
            self.n_cz.append(n_cz*(2*i+3))
            self.experiment_adds.append(Experiment(
                    self.experiment._settings,
                    prog,
                    symmetrization=self._symmetrization,
                    calibration=self._calibration,
                    ))

    def set_experiment(self, experiment: Experiment):
        self.experiment = experiment

    def set_calibrations(self, calibrations):
        self.calibrations = calibrations

    def set_nterms_current(self, nterms):
        self._nterms_cur = nterms

    def get_term_es(self, results):
        term_es = np.zeros(self._nterms_cur)
        if self._term_identity is not None:
            term_es[self._term_identity] = 1.
        isum = 0
        for i, res in enumerate(results):
            if self.calibrations is not None:
                res = correct_experiment_result(res, self.calibrations[i])
            imap = self._gterm_map[isum]
            if imap >= 0:
                term_es[imap] = res.expectation.real
            isum += 1
            if res.additional_results:
                for a in res.additional_results:
                    imap = self._gterm_map[isum]
                    if imap >= 0:
                        term_es[imap] = a.expectation.real
                    else:
                        raise ValueError(f"imap = {imap} < 0!")
                    isum += 1
        return term_es

    @timeit
    def run_experiments(self, memory_map):
        results = self.qc.experiment(self.experiment,
                memory_map=memory_map)
        term_es_list = []
        term_es = self.get_term_es(results)

        term_es_list.append(term_es)
        if self.error_mitigation is not None:
            for i, expts in enumerate(self.experiment_adds):
                results = self.qc.experiment(expts,
                        memory_map=memory_map)
                term_es = self.get_term_es(results)
                term_es_list.append(term_es)
        self.update_term_es(term_es_list)

    def update_term_es(self, term_es_list):
        term_es = term_es_list[0]
        if len(term_es_list) > 1:
            term_es_list = np.asarray(term_es_list).T
            for i, vals in enumerate(term_es_list):
                f = interpolate.interp1d(self.n_cz, vals,
                        fill_value='extrapolate')
                term_es[i] = f(0)
        if (self.term_es is None) or (not self._append_mode):
            self.term_es = term_es
        else:
            self.term_es = np.concatenate((self.term_es, term_es))

    def evaluate_expval(self, memory_map):
        # to evaluate the objective function only.
        if self.experiment is None:
            self.compile_tomo_expts()
        self.run_experiments(memory_map)
        res = sum([term.coefficient*self.term_es[i] for
                i, term in enumerate(self.pauli_list)])
        return res.real

    def set_tomo_shots(self, shots):
        """
        Set the number of shots for the tomography experiment.
        Warning: requires recompilation of the circuits.

        :param int shots: number of tomography repetitions.

        """
        self.shots = shots
