from pyquil.quil import Program
from pyquil.gates import CZ
from pyquil.quilbase import Pragma


# conveineince function
def order_pauli_label(label):
    if label == "I":
        return label
    gates = []
    qbits = []
    s_num = ""
    for s in label:
        if s.isdigit():
            s_num += s
        else:
            gates.append(s)
            if s_num != "":
                qbits.append(int(s_num))
                s_num = ""
    qbits.append(int(s_num))
    assert(len(qbits) == len(gates)),  \
            f"pauli label {label} gate-qubit site not consistent!"
    label = "".join([f"{g}{q}" for q, g in sorted(zip(qbits, gates))])
    return label


def get_term_labels_from_file(fname):
    '''read terms from text file with lines like
    1.23 X0Y3Z5
    '''
    terms = []
    with open(fname, "r") as f:
        for line in f:
            line = line.split()
            if len(line) >= 1:
                line = line[-1].rstrip('\n')
                label = order_pauli_label(line)
                terms.append(f"1*{label}")
    return terms


def get_pauli_term_index(term, nq, cq=None):
    '''
    get the index of the pauli term in the full array.
    '''
    if cq is None:
        cq = list(range(nq))
    else:
        assert(len(cq) == nq), f"len(cq) {len(cq)} not match with {nq}!"
    idx = 0
    oneindex = {"I": 0, "X": 1, "Y": 2, "Z": 3}
    for i, q in enumerate(cq):
        s = term._ops.get(q, "I")
        idx += 4**i*oneindex[s]
    return idx


def get_term_label(slabel, cq):
    '''convert label 'IXYZ' to label 'X1Y2Z3'.
    '''
    tlabel = "1*"+"".join(f"{s}{cq[j]}" for j, s
            in enumerate(slabel[::-1]) if s != "I")
    if tlabel == "1*":
        tlabel += "I"
    return tlabel


def complete_term_label_list(nq, cq=None):
    '''generate completer term label list for nq-qubit system.
    '''
    if cq is None:
        cq = list(range(nq))
    pauli_list = ["I","X","Y","Z"]
    from itertools import product
    for label in product(pauli_list, repeat=nq):
        yield get_term_label(label, cq)


def add_pairs_cz(prog, n):
    '''
    add n pairs of cnots to each cnot in prog to increase noise level.
    '''
    n_cz = 0
    prog2 = Program()
    for p in prog:
        prog2 += p
        if hasattr(p, "name"):
            if p.name == "CNOT" or p.name == "CZ":
                n_cz += 1
                q1, q0 = p.get_qubits()
                prog2 += Pragma("PRESERVE_BLOCK")
                for i in range(2*n):
                    prog2 += CZ(q0, q1)
                prog2 += Pragma("END_PRESERVE_BLOCK")
    prog2.wrap_in_numshots_loop(shots=prog.num_shots)
    return prog2, n_cz


def get_pauli_term_with_cq(term, cq):
    '''
    modify pauli term with customer qubits.
    '''
    if cq is not None:
        ops = term._ops.copy()
        # clear ordereddict
        for key, val in ops.items():
            term._ops.popitem()
        for key, val in ops.items():
            term._ops[cq[key]] = val
    return term


def get_pauli_term_without_cq(term, cq):
    '''
    retrieve pauli term without customer qubits.
    '''
    if cq is not None:
        ops = term._ops.copy()
        # clear ordereddict
        for key, val in ops.items():
            term._ops.popitem()
        for key, val in ops.items():
            term._ops[cq.index(key)] = val
    return term
