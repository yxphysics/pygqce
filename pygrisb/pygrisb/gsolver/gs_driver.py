def driver(imp=0, iembeddiag=-1, chk_argv=True, path="./",
        mpiexec=["mpirun", "-np", "1"], nval_bot=0,
        nval_top=None, edinit=0, cygtail="", jac=None):
    if chk_argv:
        import argparse, os
        g_root = os.environ['WIEN_GUTZ_ROOT']
        parser = argparse.ArgumentParser()
        parser.add_argument("-i", "--imp", type=int, default=imp,
                help="impurity index zero-based (int)")
        parser.add_argument("-d", "--iembeddiag", type=int, default=None,
                help="choice embedding h solver (int)")
        parser.add_argument("-m", "--mpi", type=str, default="mpirun -np 1",
                help="mpirun prefix (str)")
        parser.add_argument("-p", "--path", type=str, default=g_root,
                help="executible path (str)")
        parser.add_argument("-vb", "--valbot", type=int, default=nval_bot,
                help="valence bottom (int)")
        parser.add_argument("-vt", "--valtop", type=int, default=nval_top,
                help="valence top (int)")
        args = parser.parse_args()
        args.mpi = args.mpi.split()
        imp = args.imp
        iembeddiag = args.iembeddiag
        mpiexec = args.mpi
        path = args.path
        nval_bot = args.valbot
        nval_top = args.valtop

    if iembeddiag == -10:
        from pygrisb.gsolver.gs_ml import gs_h5ml_fsoc_krr
        gs_h5ml_fsoc_krr(imp=imp, jac=jac)

    elif iembeddiag == -11:
        from pygrisb.gsolver.gs_ml import gs_h5ml_fsoc_nm1d
        gs_h5ml_fsoc_nm1d(imp=imp, jac=jac)

    elif iembeddiag == -13:
        from pygrisb.gsolver.gs_ml import gs_h5ml_fsoc_nm2d
        gs_h5ml_fsoc_nm2d(imp=imp, jac=jac)

    elif iembeddiag in [-1, -2, -3, -4]:
        import subprocess
        cmd = [f'{path}/exe_spci{cygtail.lower()}', '-i', str(imp+1), '-m', \
                str(-iembeddiag), '-e', str(edinit)]
        subprocess.run(cmd)

    elif iembeddiag == 1:
        from pygrisb.gsolver.gsolver import gsolver_h5trans_ed
        gs = gsolver_h5trans_ed(imp=imp, mpiexec=mpiexec, path=path,
                nval_bot=nval_bot, nval_top=nval_top)
        gs.driver()

    elif iembeddiag == 101:
        from pygrisb.gsolver.gsolver import gsolver_h5ed
        gs = gsolver_h5ed(imp=imp, mpiexec=mpiexec, path=path,
                nval_bot=nval_bot, nval_top=nval_top)
        gs.driver()

    elif iembeddiag == 20:
        from pygrisb.gsolver.gs_qc import gsolver_qiskit_ed
        gs = gsolver_qiskit_ed(imp=imp)
        gs.driver()

    elif iembeddiag == 21:
        from pygrisb.gsolver.gs_qc import gsolver_qiskit_uccsd_statvec
        gs = gsolver_qiskit_uccsd_statvec(imp=imp)
        gs.driver()

    elif iembeddiag == 22:
        from pygrisb.gsolver.gs_qc import gsolver_qiskit_uccsd_qasm
        gs = gsolver_qiskit_uccsd_qasm(imp=imp)
        import multiprocessing
        gs.evaluate(ncore=multiprocessing.cpu_count())
        gs.save_results()

    elif iembeddiag == 24:
        from pygrisb.gsolver.gs_qc import gsolver_qiskit_uccsd_qasm
        gs = gsolver_qiskit_uccsd_qasm(imp=imp)
        gs.load_account()
        backend = gs.get_backend("ibmq_qasm_simulator")
        gs.evaluate(
                backend=backend,
                ncore=0,
                )
        gs.save_results()

    elif iembeddiag == 23:
        from pygrisb.gsolver.gs_qc import gsolver_qiskit_uccsd_qasm
        from qiskit.providers.aer import noise
        gs = gsolver_qiskit_uccsd_qasm(imp=imp)
        gs.load_account()
        device = gs.get_backend("ibmq_essex")
        coupling_map = device.configuration().coupling_map
        noise_model = noise.device.basic_device_noise_model(
                device.properties())
        import multiprocessing
        from qiskit.ignis.mitigation.measurement import CompleteMeasFitter
        gs.evaluate(
                shots=2**13,
                noise_model=noise_model,
                coupling_map=coupling_map,
                measurement_error_mitigation_cls=CompleteMeasFitter,
                ncore=multiprocessing.cpu_count(),
                )
        gs.save_results()

    elif iembeddiag == 30:
        from pygrisb.gsolver.gs_qc import gsolver_qiskit_uccsd_qasm
        gs = gsolver_qiskit_uccsd_qasm(imp=imp)
        gs.load_account()
        backend = gs.get_backend("ibmq_essex")
        from qiskit.ignis.mitigation.measurement import CompleteMeasFitter
        gs.evaluate(
                shots=2**13,
                backend=backend,
                measurement_error_mitigation_cls=CompleteMeasFitter,
                )
        gs.save_results()

    elif iembeddiag == 39:
        from pygrisb.gsolver.forest.gs import gsolver_vqe_wfn
        gs = gsolver_vqe_wfn(imp=imp)
        gs.driver()

    elif iembeddiag == 40:
        from pygrisb.gsolver.forest.gs import gsolver_vqe_qc
        gs = gsolver_vqe_qc(imp=imp)
        gs.driver()
    else:
        raise NotImplementedError( \
                f"iembeddiag = {iembeddiag} not available!")
