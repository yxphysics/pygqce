from pygrisb.run.timing import timeit
import os, numpy, h5py, warnings
from collections import deque
from pyquil.paulis import PauliTerm
import qc_ansatz as qca
from pyquil.experiment._symmetrization import SymmetrizationLevel
from pyquil.experiment._calibration import CalibrationMethod
from pygrisb.gsolver.forest._ansatz import input_ansatz
from pygrisb.gsolver.forest.util import get_pauli_term_with_cq, \
        get_pauli_term_index


class qite_base:
    '''
    base class of qite.
    '''
    def __init__(self,
            hop,  # list of pauli terms for the hamiltonian
            imp=0,  # impurity index
            db=0.5,  # time step size
            maxiter=40,  # max time steps
            continuous_run=False,  # continuous run given prevous solution.
            init_eval=0,  # initial evaluation method.
            auto_tune_step=True,  # auto tune step size.
            ):
        self._hop = hop
        self._imp = imp
        self._db = db
        self._maxiter = maxiter
        self._elist = []
        self._crun = continuous_run
        self._init_eval = init_eval
        self._auto_tune_step = auto_tune_step
        self._a = None
        self._param_expt = None
        self.file_input()

    def file_input(self):
        # order ocnvention is reversed.
        self._istate = qca.ref_bitstrings[self._imp][::-1]
        self._nq = len(self._istate)
        self._ansatz_ops = [PauliTerm.from_compact_str(label) for label in
                qca.ansatz_ops[self._imp]] # not tied to specific qubit order
        self._maxiter = getattr(qca, "maxiter", self._maxiter)
        self._db = getattr(qca, "db", self._db)
        self._auto_tune_step = getattr(qca, "auto_tune_step",
                self._auto_tune_step)
        self._crun = getattr(qca, "continuous_run", self._crun)
        self._init_eval = getattr(qca, "init_eval", self._init_eval)

    def get_updated_a(self, delta=1.e-4):
        '''update a-coefficients.
        '''
        delta = getattr(qca, "delta", delta)
        s = self.get_s()
        b = self.get_b()
        # with regularization
        smat = numpy.identity(s.shape[0], dtype=numpy.float)*delta
        smat += s + s.T
        res = numpy.linalg.lstsq(smat, -b, rcond=-1)
        return res[0]

    def get_s(self):
        return self._s

    def get_b(self):
        return self._b

    def get_expval_init_ps(self, term):
        '''
        get expectation value of pauli terms with respect to the initial
        simple product state (0011 ...).
        '''
        res = term.coefficient
        if abs(res) < 1.e-8:
            res = 0
        else:
            if "X" in term._ops.values() or "Y" in term._ops.values():
                return 0  # zero in simple product state
            for i, q in enumerate(qca.cq):
                s = term._ops.get(q, "I")
                if s == "Z" and self._istate[i] == "1":
                    res *= -1
        return res

    def h5save_result(self, fname="result.h5"):
        with h5py.File(fname, "w") as f:
            f["/e_list"] = self._elist
            if self._a is not None:
                f["/a_db"] = self._a*self._db

    def load_a(self):
        if self._crun:
            if os.path.isfile("result.h5"):
                with h5py.File("result.h5", "r") as f:
                    if "/a_db" in f:
                        a = f["/a_db"][()]
                        if a.shape == self._b.shape:
                            self._a = a/self._db
                            print(f"a loaded: cotinuous run.")
                            if self._init_eval == 0:
                                self._init_eval = 1


class qite_mstep(qite_base):
    '''
    qite with steps merged.
    '''
    @timeit
    def set_init_arrays(self):
        # S_IJ mapping indices
        nterms = len(self._ansatz_ops)
        # set up a complete list of relevant pauli terms
        ntermh = len(self._hop)
        self._full_term_list = []
        # tracking array
        tracking = -numpy.ones([4**self._nq], dtype=numpy.int)
        term_identity = None
        # Hamiltonian
        self._i_h = numpy.zeros([ntermh], dtype=numpy.int)
        for i, term in enumerate(self._hop):
            term1 = term.copy()
            term1.coefficient = complex(1.)
            idx = get_pauli_term_index(term1, self._nq)
            if tracking[idx] >= 0:
                warnings.warn((f"term {term1} appeared more then once in h!"))
                self._i_h[i] = tracking[idx]
            else:
                n = len(self._full_term_list)
                tracking[idx] = n
                self._i_h[i] = n
                self._full_term_list.append(term1)
                if idx == 0 and term_identity is None:
                    term_identity = n
        print(f"n_terms in H: {len(self._full_term_list)}")

        # print("setting s_ij")
        self._ij0_s = numpy.zeros([nterms, nterms], dtype=numpy.int)
        self._ij1_s = numpy.zeros([nterms, nterms], dtype=numpy.complex)
        for i, termi in enumerate(self._ansatz_ops):
            for j, termj in enumerate(self._ansatz_ops[:i+1]):
                termij = termi*termj
                self._ij1_s[i, j] = termij.coefficient
                termij.coefficient = complex(1.)
                idx = get_pauli_term_index(termij, self._nq)
                if tracking[idx] >= 0:
                    self._ij0_s[i, j] = tracking[idx]
                else:
                    n = len(self._full_term_list)
                    tracking[idx] = n
                    self._ij0_s[i, j] = n
                    self._full_term_list.append(termij)
                    if idx == 0 and term_identity is None:
                        term_identity = n

        # print("setting i_b")
        # h*term index
        self._hi0_b = numpy.zeros([ntermh, nterms], dtype=numpy.int)
        self._hi1_b = numpy.zeros([ntermh, nterms], dtype=numpy.complex)
        for h, termh in enumerate(self._hop):
            for i, termi in enumerate(self._ansatz_ops):
                termhi = termh*termi
                self._hi1_b[h, i] = termhi.coefficient
                termhi.coefficient = complex(1.)
                idx = get_pauli_term_index(termhi, self._nq)
                if tracking[idx] >= 0:
                    self._hi0_b[h, i] = tracking[idx]
                else:
                    n = len(self._full_term_list)
                    tracking[idx] = n
                    self._hi0_b[h, i] = n
                    self._full_term_list.append(termhi)
                    if idx == 0 and term_identity is None:
                        term_identity = n

        print(f"n_terms in total: {len(self._full_term_list)}")
        # <\Psi_n | \sigma_I^\dagger \sigma_J | \Psi_n>
        self._s = numpy.zeros([nterms, nterms], dtype=numpy.float)
        # i c_n^(-1/2)<\Psi_n|H \sigma_I|\Psi_n> + c.c.
        self._b = numpy.zeros([nterms], dtype=numpy.float)
        self._term_map = tracking
        self._term_identity = term_identity
        self.load_a()

    @timeit
    def set_init_expvals(self):
        self._full_term_expvals = [self.get_expval_init_ps(term)
                for term in self._full_term_list]

    def set_e(self):
        e = sum([self._full_term_expvals[self._i_h[i]]*term.coefficient \
                for i, term in enumerate(self._hop)])
        self._e = e.real
        self._elist.append(self._e)

    def set_sb(self, ratio=5):
        nterms = len(self._ansatz_ops)
        ntermh = len(self._hop)
        for i in range(nterms):
            for j in range(i+1):
                ij, coefij = self._ij0_s[i, j], self._ij1_s[i, j]
                zes = coefij*self._full_term_expvals[ij]
                self._s[i, j] = zes.real
                if i != j:
                    self._s[j, i] = self._s[i, j]
            bi = 0
            for h in range(ntermh):
                hi, coef = self._hi0_b[h, i], self._hi1_b[h, i]
                zes = coef*self._full_term_expvals[hi]
                bi -= zes.imag*2
            self._b[i] = bi
        while True:
            cn = 1 - 2*self._db*self._e
            if cn <= 0:
                self._db /= ratio
                warnings.warn(f"mandatory reduce db to {self._db}")
            else:
                break
        self._b /= numpy.sqrt(cn)

    @timeit
    def one_iteration(self, istep):
        self.set_sb()
        a_new = self.get_updated_a()
        if self._a is None:
            self._a = a_new
        else:
            self._a += a_new
        self.set_expvals()
        self.set_e()
        print(f"{istep:2d}: e = {self._e}")
        # self tuning
        iflag = self.adjust_db()
        return iflag

    def adjust_db(self, ratio=5, maxlen=2):
        '''self-adjusting db.
        '''
        if not hasattr(self, "_a_best"):
            self._a_best = deque(maxlen=maxlen)
            self._e_best = deque(maxlen=maxlen)
            self._full_term_expvals_best = deque(maxlen=maxlen)
            self._e_best.append(self._e)
            self._a_best.append(self._a)
            if self._a is not None:
                self._a_final = self._a*self._db
            else:
                self._a_final = None
            self._e_final = self._e
            self._full_term_expvals_best.append(
                    self._full_term_expvals.copy())
        else:
            # record the absolute minimal
            if self._e < self._e_final:
                self._e_final = self._e
                self._a_final = self._a*self._db

            # record info for self-tuning db.
            if self._e < self._e_best[-1]:
                self._e_best.append(self._e)
                self._a_best.append(self._a.copy())
                self._full_term_expvals_best.append(
                        self._full_term_expvals.copy())
            elif self._auto_tune_step:
                if self._a_best[0] is not None:
                    self._a_best[0] *= ratio
                self._db /= ratio
                self._a = self._a_best[0]
                self._e = self._e_best[0]
                self._full_term_expvals = \
                        self._full_term_expvals_best[0]
                print(f"db adjusted to {self._db:.1e}")
                print(f"revert to e: {self._e}")
                n = len(self._e_best)
                for _ in range(n-1):
                    self._a_best.pop()
                    self._e_best.pop()
                if self._db < 1.e-4:
                    self._elist.append(self._e)
                    return -1
        return 0

    def final_adjust(self):
        if self._a_final is not None:
            self._a = self._a_final/self._db
        self._e = self._e_final
        print(f"final energy: {self._e}")

    @timeit
    def run(self, nswitch=0, tol=1.e-6):
        """
        main routine.
        """
        self.set_init_arrays()
        if self._init_eval == 0:
            self.set_init_expvals()
        else:
            self.set_expvals()
        self.set_e()

        # self tuning
        self.adjust_db()

        print(f"init: e = {self._e}")
        print(f"      db = {self._db}")
        for i in range(self._maxiter):
            iflag = self.one_iteration(i)
            if iflag < 0:
                break
        self.final_adjust()
        self.h5save_result()


class qite_mstep_wfn(qite_mstep):
    '''
    using wavefunction simulator
    '''
    def file_input(self):
        super().file_input()
        from pygrisb.gsolver.forest.paramexpt import paramExperimentWFN
        self._param_expt = paramExperimentWFN(
                hamiltonian=self._hop,
                )
        # check exact energy
        print('Exact ground state energy:', self._param_expt.get_exact_gs())
        # load initial state, by convention, cq already applied
        self._param_expt.set_ref_state(qca.ref_states[self._imp])
        print('initial circuit energy estimate: ',
                self._param_expt.objective_function())

        # load ansatz, no custom_qubits needed
        ansatz_circuit = input_ansatz(self._imp)
        self._param_expt.set_ansatz(ansatz_circuit)

    @timeit
    def set_expvals(self):
        if self._a is None:
            a = numpy.zeros_like(self._b)
        else:
            a = self._a*self._db*(-2)
        memory_map = {'theta': a}
        self._full_term_expvals = self._param_expt.evaluate_expval(memory_map,
               terms=self._full_term_list).tolist()

    def get_expval(self, term):
        if self._a is None:
            a = numpy.zeros_like(self._b)
        else:
            a = self._a*self._db*(-2)
        memory_map = {'theta': a}
        res = self._param_expt.evaluate_expval(memory_map,
               terms=term)
        return res

    #def get_qite_ed_wf_ovlp(self):
    #    wf_ed = self._param_expt.get_exact_gs(mode="v")
    #    memory_map = {'theta': self._a*self._db*(-2)}
    #    wf_qite = self._param_expt.get_wavefunction(memory_map)
    #    # need to check basis convention.

class qite_mstep_qc(qite_mstep):
    '''
    using wavefunction simulator
    '''
    def file_input(self):
        super().file_input()
        from pygrisb.gsolver.forest.paramexpt import  \
                paramExperimentQC
        self._param_expt = paramExperimentQC(
                qc=qca.qc,
                hamiltonian=self._hop,
                shots=qca.shots,
                custom_qubits=qca.cq,
                symmetrization=getattr(qca, "symmetrization",
                        SymmetrizationLevel.EXHAUSTIVE),
                calibration=getattr(qca, "calibration",
                        CalibrationMethod.PLUS_EIGENSTATE),
                error_mitigation=getattr(qca, "error_mitigation", None),
                verbose=qca.verbose,
                )
        # load initial state
        self._param_expt.set_ref_state(qca.ref_states[self._imp])
        # load ansatz
        ansatz_circuit = input_ansatz(self._imp)
        self._param_expt.set_ansatz(ansatz_circuit)

    @timeit
    def set_init_arrays(self):
        super().set_init_arrays()
        # compile tomographic experiments.
        # cq_appled pauli terms for experiments
        terms_cq = [get_pauli_term_with_cq(term, qca.cq)
                for term in self._full_term_list]
        self._param_expt.compile_tomo_expts(
                input_list=[terms_cq, self._term_identity, self._term_map])

    def set_expvals(self):
        memory_map = {'theta': self._a*self._db*(-2)}
        self._param_expt.run_experiments(memory_map)
        self._full_term_expvals = self._param_expt.term_es


def qite_mstep_wfn_from_file(tol=1.e-6):
    # read in Hamiltonian in text file.
    with open("h.inp", "r") as f:
        hop = [PauliTerm.from_compact_str(label.rstrip("\n").rstrip())
                for label in f if abs(float(label.split("*")[0])) >= tol]
    qite = qite_mstep_wfn(hop)
    qite.run()
    return qite


def qite_mstep_qc_from_file():
    # read in Hamiltonian in text file.
    with open("h.inp", "r") as f:
        hop = [PauliTerm.from_compact_str(label.rstrip("\n").rstrip())
                for label in f]
    qite = qite_mstep_qc(hop)
    qite.run()
    return qite
