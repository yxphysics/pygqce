from pygrisb.run.timing import timeit
import numpy, h5py, os
from pyquil.paulis import PauliTerm
from pygrisb.gsolver.forest._ansatz import input_ansatz
from pygrisb.gsolver.forest.util import complete_term_label_list


class vqe_base:
    '''
    base class of vqe.
    '''
    def __init__(self),
            hop,  # list of pauli terms for the hamiltonian
            qca,  # qc_ansatz
            imp=0,
            ):
        self._hop = hop
        self._imp = imp
        self._elist = []
        self._xlist = []
        self._param_expt = None
        # order ocnvention is reversed.
        self._istate = qca.ref_bitstrings[self._imp][::-1]
        self._nq = len(self._istate)
        self._ansatz_ops = [PauliTerm.from_compact_str(label) for label in
                qca.ansatz_ops[imp]] # not tied to specific qubit order
        self._maxiter = getattr(qca, "maxiter", self._maxiter)
        self.setup_expt(qca)

    def h5save_result(self, fname="result.h5"):
        with h5py.File(fname, "w") as f:
            f["/e_list"] = self._elist
            f["/a"] = self._a


class vqe_wfn(vqe_base):
    '''
    using wavefunction simulator.
    '''
    def setup_expt(self, qca):
        from pygrisb.gsolver.forest.paramexpt import paramExperimentWFN
        self._param_expt = paramExperimentWFN(
                hamiltonian=self._hop,
                )
        # check exact energy
        print('Exact ground state energy:', self._param_expt.get_exact_gs())
        # load initial state, by convention, cq already applied
        self._param_expt.set_ref_state(qca.ref_states[self._imp])
        print('initial circuit energy estimate: ',
                self._param_expt.objective_function())

        # load ansatz, no custom_qubits needed
        ansatz_circuit = input_ansatz(self._imp)
        self._param_expt.set_ansatz(ansatz_circuit)

    @timeit
    def set_expvals(self):
        memory_map = {'theta': self._a*self._db*(-2)}
        self._full_term_expvals = self._param_expt.evaluate_expval(memory_map,
               terms=self._full_term_list)

class qite_cstep_qc(qite_cstep):
    '''
    using wavefunction simulator
    '''
    def file_input(self):
        super().file_input()
        from pygrisb.gsolver.forest.paramexpt import  \
                paramExperimentQCFullExpval
        self._param_expt = paramExperimentQCFullExpval(
                qc=qca.qc,
                hamiltonian=self._hop,
                shots=qca.shots,
                custom_qubits=qca.cq,
                error_mitigation=getattr(qca, "error_mitigation", None),
                verbose=qca.verbose,
                )
        # load initial state
        self._param_expt.set_ref_state(qca.ref_states[self._imp])
        # load ansatz
        ansatz_circuit = input_ansatz(self._imp)
        self._param_expt.set_ansatz(ansatz_circuit)
        # cq_appled pauli terms for experiments
        terms = [PauliTerm.from_compact_str(label)
                for label in complete_term_label_list(self._nq, cq=qca.cq)]
        self._param_expt.compile_tomo_expts(pauli_list=terms)

    def set_expvals(self):
        memory_map = {'theta': self._a*self._db*(-2)}
        self._param_expt.run_experiments(memory_map)
        self._full_term_expvals = self._param_expt.term_es
