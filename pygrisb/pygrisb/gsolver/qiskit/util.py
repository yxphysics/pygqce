def save_weighted_paulis(wpaulis, fname, tol=1.e-8):
    '''save in text file to be used by forest sdk.
    '''
    with open(fname, "w") as f:
        for weight, pauli in wpaulis._paulis:
            if abs(weight) > tol:
                label = pauli.to_label()
                labels = "".join(f"{s}{i}" for i,s in enumerate(label) \
                        if s != "I")
                if labels == "":
                    labels = "I"
                f.write(f"{weight.real:.8f}*{labels}\n")
