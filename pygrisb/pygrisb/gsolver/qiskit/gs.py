# have to be first line
from qiskit import Aer
import numpy
from pygrisb.gsolver.gsolver import gsolver_h5
from pygrisb.math.matrix_util import unitary_transform_coulomb_matrix
from pygrisb.run.timing import timeit

from pyscf import gto, scf, ao2mo
from qiskit.aqua import QuantumInstance
from qiskit.aqua.algorithms import VQE, ExactEigensolver
from qiskit.chemistry import FermionicOperator
from qiskit.chemistry.components.variational_forms import UCCSD
from qiskit.chemistry.components.initial_states import HartreeFock
from qiskit.aqua.operators.weighted_pauli_operator import Z2Symmetries
from qiskit import IBMQ



class gsolver_qiskit(gsolver_h5):
    '''base class of Gutzwiller solver with qiskit.
    '''
    def __init__(self,
            imp=0,
            map_type="parity",  # jordan_wigner, parity, bravyi_kitaev
            qubit_reduction=None,
            ao2mo=True,
            ):
        super().__init__(imp=imp, jac=None, read_v2e=True)
        self.set_parameters()
        self.set_full_v12e()
        self.reduce_spin_v12e()
        # type to map fermionic operator to qubit operator
        # e.g., jordan_wigner, parity, bravyi_kitaev (binary coding), etc.
        self.map_type = map_type
        # paramagnetic solution without spin-orbit coupling.
        # v1e, v2e is only for one spin block here.
        # half-filling defined by the gutzwiller theory.
        self.num_electrons = self.v1e.shape[0]
        # include factor of 2 from spin.
        self.num_spin_orbitals = self.v1e.shape[0]*2
        # ao representation to mo representation
        self._ao2mo = ao2mo
        # whether allow qubit reduction
        if qubit_reduction is None:
            if  map_type == "parity":
                self.qubit_reduction = True
            else:
                self.qubit_reduction = False
        else:
            self.qubit_reduction = qubit_reduction

    def reduce_spin_v12e(self):
        '''reduce v1e, v2e to one spin component.
        spin-index faster.
        '''
        self.v1e = self.v1e[::2, ::2]
        self.v2e = self.v2e[::2, ::2, ::2, ::2]

    def load_account(self):
        # load IBM account
        if IBMQ.active_account() is None:
            IBMQ.load_account()

    @staticmethod
    def check_backends():
        provider = IBMQ.get_provider()
        backends = provider.backends(simulator=False)
        gsolver_qiskit.print_backends("ibm q devices", backends)
        backends = provider.backends(simulator=True)
        gsolver_qiskit.print_backends("ibm q simulators", backends)
        backends = Aer.backends()
        gsolver_qiskit.print_backends("local simulators", backends)

    @staticmethod
    def print_backends(title, backends):
        print(f"\navailable {title}:")
        for backend in backends:
            print(backend)

    @staticmethod
    def get_backend(backend="qasm_simulator"):
        if backend == "qasm_simulator" or backend == "statevector_simulator":
            return Aer.get_backend(backend)
        elif backend[:4] == "ibmq":
            provider = IBMQ.get_provider(hub='ibm-q')
            return provider.get_backend(backend)
        else:
            raise NotImplementedError("backend not available!")

    def get_qubit_operator(self,
            h1,  # one-body parameters
            h2=None,  # two-body parameters
            threshold=1.e-10):  # threshold to chop negligible terms.
        # get the qubit operator from fermionic operator defined by
        # spin-included one-body h1 and two-body h2 part.
        # sanity check
        assert(h1.shape[0] == self.v1e.shape[0]*2), \
                "h1 should include spin component!"
        # get fermionic operators
        fop = FermionicOperator(h1=h1, h2=h2)
        # map to qubit operator according to map_type
        qop = fop.mapping(map_type=self.map_type, threshold=threshold)
        # It can be used to eliminate two qubits in parity
        # and binary-tree mapped fermionic Hamiltonians
        # when the spin orbitals are ordered in two spin
        # sectors, (block spin order) according to the number of particles in
        # the system.
        if self.qubit_reduction:
            qop = Z2Symmetries.two_qubit_reduction(qop, self.num_electrons)
        qop.chop(threshold)
        return qop

    def get_hf_integrals(self):
        n = self.num_spin_orbitals//2
        if self._ao2mo:
            # given one-body and two-body (one spin-block)
            # part of the Hamiltonian,
            # get the hartree-fock (hf) orbital and parameters in the
            # hf orbital basis incluidng spin component.
            # get am empty molecule.
            mol = gto.M()
            mol.nelectron = self.num_electrons
            # setup hartree-fock calculation
            mf = scf.RHF(mol)
            # function to get one-body part
            mf.get_hcore = lambda *args: self.v1e
            # basis overlap matrix
            mf.get_ovlp = lambda *args: numpy.eye(n)
            # get 8-fold permutation symmetry of the integrals
            mf._eri = ao2mo.restore(8, self.v2e, n)
            # hartree-fock self-consistent calculation.
            mf.kernel()
            # one and two-body integrals with mo
            u = mf.mo_coeff
            mohij = u.T.dot(self.v1e).dot(u)
            movijkl = unitary_transform_coulomb_matrix(self.v2e, u)
        else:
            u = numpy.eye(n)
            mohij = self.v1e.copy()
            movijkl = self.v2e.copy()
        # adding spin components
        ns = self.num_spin_orbitals
        mohsij = numpy.zeros([ns, ns], dtype=mohij.dtype)
        mohsij[:n, :n] = mohsij[n:, n:] = mohij
        movsijkl = numpy.zeros([ns, ns, ns, ns], dtype=movijkl.dtype)
        # factor 0.5 included by convention.
        # convention of qiskit (qiskit-aqua/qiskit/chemistry/qmolecule.py).
        movsijkl[:n, :n, :n, :n] = movsijkl[:n, :n, n:, n:] = \
                movsijkl[n:, n:, :n, :n] = \
                movsijkl[n:, n:, n:, n:] = 0.5*movijkl
        return u, mohsij, movsijkl

    def get_dm_mo_qops(self):
        # get the qubit operators for one particle density matrix in mo basis.
        ns = self.num_spin_orbitals
        n = ns//2
        qops = []
        # by spin-symmetry, only one spin block of density matrix is needed.
        for i in range(n):
            for j in range(i+1):
                # for dm[i,j]
                h1 = numpy.zeros([ns, ns])
                h1[i, j] = 1.0
                qop = self.get_qubit_operator(h1)
                qops.append(qop)
        return qops

    def set_dm(self, dm_mo, mo_coeff):
        # given the density matrix in mo basis,
        # get it in atomic orbital basis using mo_coeff.
        dm = numpy.zeros_like(mo_coeff)
        ns = self.num_spin_orbitals
        n = ns//2
        for i in range(n):
            for j in range(i+1):
                dm[i,j] = dm_mo.pop(0)
                if i != j:
                    dm[j,i] = dm[i,j]
        dm = mo_coeff.dot(dm).dot(mo_coeff.T)
        self._dm = numpy.zeros((ns, ns), dtype=dm.dtype)
        # spin-index faster
        self._dm[::2, ::2] = self._dm[1::2, 1::2] = dm

    def get_h_mo_qop(self):
        # get the qubit operator of hamiltonian in mo basis.
        # get integrals in molecular spin-orbital basis.
        mo_coeff, mo_h1, mo_h2 = self.get_hf_integrals()
        # get qubit operators of the hamiltonian
        h_qop = self.get_qubit_operator(mo_h1, mo_h2)
        return mo_coeff, h_qop

    @timeit
    def evaluate(self):
        # get qubit operator for the hamiltonain
        mo_coeff, h_qop = self.get_h_mo_qop()
        # get qubit operators fo the density matrix elements.
        dm_qops = self.get_dm_mo_qops()
        dm_mo = self.solve(h_qop, dm_qops)
        self.set_dm(dm_mo, mo_coeff)

    def solve(self):
        raise NotImplementedError("solve not implemented!")


def save_h12(h1, h2):
    import h5py
    with h5py.File("mol_hamil.h5", "w") as f:
        f["h1_mol"] = h1
        f["h2_mol"] = h2


class gsolver_qiskit_ed(gsolver_qiskit):
    '''Gutzwiller solver with qiskit using exact diagonalization.
    '''
    def solve(self, h_qop, dm_qops):
        exact_eigensolver = ExactEigensolver(h_qop, aux_operators=dm_qops)
        ret = exact_eigensolver.run()
        self._emol = ret['eigvals'][0].real
        return ret['aux_ops'][0,:,0].tolist()


class gsolver_qiskit_uccsd(gsolver_qiskit):
    '''Gutzwiller solver with qiskit using ccsd quantum variational solver.
    '''
    def set_var_form(self, h_qop):
        # setup hartree fock reference state
        hf_state = HartreeFock(h_qop.num_qubits, self.num_spin_orbitals,
                self.num_electrons, self.map_type, self.qubit_reduction)

        # setup UCCSD variational form
        var_form = UCCSD(h_qop.num_qubits,
                depth=1, # number of replica of basic module
                num_orbitals=self.num_spin_orbitals,
                num_particles=self.num_electrons,
                initial_state=hf_state,
                qubit_mapping=self.map_type,
                two_qubit_reduction=self.qubit_reduction)

        # special simplification for hydrogen dimer
        if self.num_spin_orbitals == 4:
            # remove single excitation terms
            var_form._hopping_ops = var_form._hopping_ops[2:]
            # keep only one double excitation term
            var_form._hopping_ops[0]._paulis[0][0] *= 2.0
            var_form._hopping_ops[0]._paulis[1][0] = 0.0
            var_form._hopping_ops[0] = var_form._hopping_ops[0].chop()
            var_form._num_parameters = 1
            var_form._bounds = var_form._bounds[2:]
        return var_form


class gsolver_qiskit_uccsd_statvec(gsolver_qiskit_uccsd):
    '''Gutzwiller solver with qiskit using ccsd quantum variational solver
    and state_vector simulator, which returns the state vector.
    '''
    def solve(self, h_qop, dm_qops, max_eval=50):
        from qiskit.aqua.components.optimizers import SLSQP
        # optimizer
        optimizer = SLSQP(maxiter=max_eval)
        var_form = self.set_var_form(h_qop)
        # setup VQE
        vqe = VQE(h_qop, var_form,
                optimizer=optimizer,
                aux_operators=dm_qops,
                )
        backend = Aer.get_backend('statevector_simulator')
        ret = vqe.run(backend)
        print(f"energy evaluated {ret['eval_count']} times.")
        self._emol = ret['eigvals'][0]
        return ret['aux_ops'][0,:,0].tolist()


class gsolver_qiskit_uccsd_qasm(gsolver_qiskit_uccsd):
    '''Gutzwiller solver with qiskit using ccsd quantum variational solver
    and qasm simulator, which returns expectation values.
    '''
    @timeit
    def evaluate(self,
            max_eval=20,  # 20 good for H2.
            shots=2**13,
            max_credits=10,
            basis_gates=None,
            coupling_map=None,
            initial_layout=None,
            optimization_level=None,
            noise_model=None,
            measurement_error_mitigation_cls=None,
            cals_matrix_refresh_period=30,
            measurement_error_mitigation_shots=None,
            ncore=1,
            backend=Aer.get_backend('qasm_simulator')
            ):
        # get qubit operator for the hamiltonain
        mo_coeff, h_qop = self.get_h_mo_qop()
        # get qubit operators fo the density matrix elements.
        dm_qops = self.get_dm_mo_qops()
        dm_mo = self.solve(h_qop, dm_qops,
                max_eval=max_eval,
                shots=shots,
                max_credits=max_credits,
                basis_gates=basis_gates,
                coupling_map=coupling_map,
                initial_layout=initial_layout,
                optimization_level=optimization_level,
                noise_model=noise_model,
                measurement_error_mitigation_cls=
                measurement_error_mitigation_cls,
                cals_matrix_refresh_period=cals_matrix_refresh_period,
                measurement_error_mitigation_shots=
                measurement_error_mitigation_shots,
                ncore=ncore,
                backend=backend,
                )
        self.set_dm(dm_mo, mo_coeff)

    def solve(self, h_qop, dm_qops,
            max_eval=10,
            shots=2**13,
            max_credits=10,
            basis_gates=None,
            coupling_map=None,
            initial_layout=None,
            optimization_level=None,
            noise_model=None,
            measurement_error_mitigation_cls=None,
            cals_matrix_refresh_period=30,
            measurement_error_mitigation_shots=None,
            ncore=1,
            backend=Aer.get_backend('qasm_simulator'),
            ):
        from qiskit.aqua.components.optimizers import SPSA
        # optimizer
        optimizer = SPSA(max_trials=max_eval)
        var_form = self.set_var_form(h_qop)
        # setup VQE
        vqe = VQE(h_qop, var_form,
                optimizer=optimizer,
                aux_operators=dm_qops,
                )
        if backend.name()[:4] == "ibmq":
            backend_options = None
        else:
            backend_options = {"max_parallel_threads": ncore,
                    "max_parallel_shots": ncore,
                    "max_parallel_experiments": ncore}
        quantum_instance = QuantumInstance(backend=backend, shots=shots,
                backend_options=backend_options,
                noise_model=noise_model,
                coupling_map=coupling_map,
                measurement_error_mitigation_cls=
                measurement_error_mitigation_cls,
                cals_matrix_refresh_period=cals_matrix_refresh_period,
                measurement_error_mitigation_shots=
                measurement_error_mitigation_shots,
                skip_qobj_validation=False,
                )
        ret = vqe.run(quantum_instance)
        print(f"energy evaluated {ret['eval_count']} times.")
        self._emol = ret['eigvals'][0]
        return ret['aux_ops'][0,:,0].tolist()
