import sys, h5py, subprocess, argparse, os
import numpy as np
from scipy import optimize
import pygrisb.run.info as info


def solve_hembed_list(
        mpi=["mpirun", "-np", "1"],
        path="./",
        iembeddiag=None,
        edinit=0,
        cygtail="",
        jac=None,
        **kwargs,
        ):
    from pygrisb.gsolver.gs_driver import driver as gs_driver
    # solve the list of inequivalent embedding Hamiltonian.
    with h5py.File("GParam.h5", "r") as f:
        imap_list = f["/"].attrs["imap_list"]
        nval_bot_list = f["/"].attrs["nval_bot_list"]
        nval_top_list = f["/"].attrs["nval_top_list"]
        _iembeddiag = f["/"].attrs["giembeddiag"]
    if iembeddiag is None:
        iembeddiag = _iembeddiag
    if iembeddiag == 10:
        # hartree-fock
        return

    for i, imap in enumerate(imap_list):
        if i==imap:
            gs_driver(imp=i, iembeddiag=iembeddiag, chk_argv=False,
                    path=path,
                    mpiexec=mpi,
                    nval_bot=nval_bot_list[i],
                    nval_top=nval_top_list[i],
                    edinit=edinit,
                    cygtail=cygtail,
                    jac=jac,
                    )

    with h5py.File("HEmbed.h5", "a") as f:
        for i, imap in enumerate(imap_list):
            if i == imap:
                if "ans" in f[f"/impurity_{i}"]:
                    del f[f"/impurity_{i}/ans"]
                with h5py.File(f"HEmbedRes_{i}.h5", "r") as fp:
                    f.copy(fp["/"], f"/impurity_{i}/ans")


def exec_cygband(
        x,
        mpi=["mpirun", "-np", "1"],
        path=".",
        cygtail="",
        **kwargs,
        ):
    # set the new trial solution vector
    with h5py.File("GIter.h5", "a") as f:
        if "/x_val" in f:
            f["/x_val"][()] = x
        if "/v_err" in f:
            del f["/v_err"]
    # calculate the updated qp bands and qp local quantities.
    cmd = mpi + [f"{path}/CyGBand{cygtail}", "-w", "0"]
    if kwargs.get("jac", False):
        cmd += ["-d", "1"]
    print(" ".join(cmd))
    subprocess.run(cmd, check=True)


def exec_cygerr(
        mpi=["mpirun", "-np", "1"],
        path=".",
        cygtail="",
        **kwargs,
        ):
    # calculate error vector
    if len(mpi) > 0:
        cmd = mpi[:-1] + ["1"]
    else:
        cmd = []
    cmd +=  [f"{path}/CyGErr{cygtail}", "-w", "0"]
    if kwargs.get("jac", False):
        cmd += ["-d", "1"]
    # get the error vector
    print(" ".join(cmd))
    subprocess.run(cmd, check=True)
    with h5py.File("GIter.h5", "r") as f:
        verr = f["/v_err"][()]
        if kwargs.get("jac", False):
            jac = f["/pfpx"][()]
        else:
            jac = None
    maxerr = np.max(np.abs(verr))
    print(f" iter = {info.iter} maxerr = {maxerr:.2e}")
    sys.stdout.flush()
    info.iter += 1
    if jac is None:
        return verr
    else:
        return verr, jac


def gfun_solver(x,
        args={"mpi":["mpirun", "-np", "1"],
                "path":".",
                "edinit":0,
                "cygtail":""},
        ):
    '''Gutzwiller vector error function. Using sparse matrix full-CI.
    '''
    # update quasiparticle bands
    exec_cygband(x, **args)
    # solve the list of inequivalent embedding Hamiltonian.
    solve_hembed_list(**args)
    # calculate error vector
    verr = exec_cygerr(**args)
    # adding absolute convergence criteria
    maxerr = np.max(np.abs(verr))
    # if total energy needed each step.
    if(maxerr < 100):
        cmd = args["mpi"] + [f"{args['path']}/CyGFinal{args['cygtail']}",
                "-r", "0"]
        print(" ".join(cmd))
        subprocess.run(cmd, check=True)

    # dot not mess up jacobian
    if args.get("adjust_verr", True):
        if maxerr < 6.e-6:
            verr *= 0
        elif maxerr < 5.e-5 and info.iter > 300:
            verr *= 0
        elif maxerr < 1.e-4 and info.iter > 400:
            verr *= 0
        elif maxerr < 1.e-3 and info.iter > 600:
            verr *= 0
    return verr


def ndlc_fun(x,
        args={"mpi":["mpirun", "-np", "1"],
                "path":"./",
                "cygtail":""},
        ):
    # update quasiparticle bands
    exec_cygband(x, **args)
    dsetname1 = args.get("dsetname1", "/d_coef")
    dsetname2 = args.get("dsetname2", "/lc_coef")
    with h5py.File("HEmbed.h5", "r") as f:
        dset1 = f[dsetname1][()]
        dset2 = f[dsetname2][()]
    verr = np.concatenate((dset1, dset2))
    return verr


def get_jacobian_analytical(
        x,
        args={"mpi":["mpirun", "-np", "1"],
                "path":"./",
                "edinit":0,
                "cygtail":""},
        ):
    exec_cygband(x, **args, jac=True)
    # solve the list of inequivalent embedding Hamiltonian.
    solve_hembed_list(**args, jac=True)
    # calculate error vector
    _, jac = exec_cygerr(**args, jac=True)
    print(" analytical jacobian called!")
    return jac


options_dict = {"hybr": {'eps':1.e-6, 'xtol':1.e-5, "factor": 1},
        "df-sane": {},
        "krylov": {},
        "excitingmixing": {
                "maxiter": 50,
                "fatol": 0.001,
                "jac_options":{"alphamax": 0.4}},
        "lm": {},
        "broyden1": {},
        "broyden2": {},
        "anderson": {},
        "linearmixing": {},
        "diagbroyden": {}}


def gsolve_nleqns(
        fun,
        method="hybr",
        args={"mpi":["mpirun", "-np", "1"], "path":"."},
        jac=None,
        tol=1.e-6,
        ):
    '''main driver to solve the set of Gutzwiller nonlinear equations.
    '''
    info.iter = 0
    # get initil solution vector
    with h5py.File("GIter.h5", "r") as f:
        if "/x_val" in f:
            x0 = f["/x_val"][()]
        else:
            x0 = None
    verr = fun(x0, args=args)
    gerr = np.max(np.abs(verr))
    if gerr > tol:
        if gerr < 1.e-4:
            # close enough, seem better
            method = "hybr"
            args["edinit"] = 0
        res = optimize.root(
                fun,
                x0,
                args=args,
                method=method,
                jac=jac,
                options=options_dict[method],
                tol=tol,
                )
        verr = res.fun
    return verr


def get_jacobian_numerical(
        x_list,
        args={"mpi":["mpirun", "-np", "1"],
        "path":"./",},
        delta=1.e-4,
        vec_fun=gfun_solver,
        ):
    jac = []
    args["adjust_verr"] = False
    vref = vec_fun(x_list, args=args)
    for i,_ in enumerate(x_list):
        xs = x_list.copy()
        xs[i] += delta
        v = vec_fun(xs, args=args)
        jac.append((v-vref)/delta)
    # recover the intial point
    with h5py.File("GIter.h5", "a") as f:
       f["/x_val"][()] = x_list
    print(" numerical jacobian called!")
    return np.asarray(jac).T


def get_inline_args():
    g_root = os.environ['WIEN_GUTZ_ROOT']
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--path", type=str, default=g_root,
            help="executible path (str)")
    parser.add_argument("-d", "--iembeddiag", type=int, default=None,
            help="choice embedding h solver (int)")
    parser.add_argument("-e", "--edinit", type=int, default=0,
            help="ed solver init guess (int)")
    parser.add_argument("-m", "--mpi", type=str, default="mpirun -np 1",
            help="mpirun prefix (str)")
    parser.add_argument("-r", "--rmethod", type=str, default="hybr",
            help="nonlinear solver method (str)")
    parser.add_argument("-j","--jac", type=int, default=0,
            help="jacobian method: 0->no, 1->analytical, -1: numerical.")
    parser.add_argument("-t","--tol", type=float, default=1e-6,
            help="tolerance (float).")
    args = parser.parse_args()
    if args.jac == -1:
        args.jac = get_jacobian_numerical
    elif args.jac == 1:
        args.jac = get_jacobian_analytical
    else:
        args.jac = None
    args.mpi = args.mpi.split()
    return args


def get_cygtail():
    with h5py.File("GParam.h5", "r") as f:
        val = f["/impurity_0/V2E"][0,0,0,0]
        if val.dtype == np.float:
            cygtail = "R"
        else:
            cygtail = ""
    return cygtail


def run_solve_hembed_list():
    args = get_inline_args()
    solve_hembed_list(
            path=args.path,
            iembeddiag=args.iembeddiag,
            edinit=args.edinit,
            mpi=args.mpi,
            cygtail=get_cygtail(),
            )


def run_cygutz():
    args = get_inline_args()
    driver_cygutz(
            path=args.path,
            rmethod=args.rmethod,
            jac=args.jac,
            mpi=args.mpi,
            iembeddiag=args.iembeddiag,
            edinit=args.edinit,
            cygtail=get_cygtail(),
            tol=args.tol,
            )


def driver_cygutz(
        path=".",
        rmethod="hybr",
        jac=None,
        mpi=["mpirun", "-np", "1"],
        iembeddiag=None,
        edinit=0,
        cygtail="",
        tol=1e-6,
        ):
    # set initial guess
    cmd = mpi + [f"{path}/CyGInit{cygtail}"]
    print(" ".join(cmd))
    subprocess.run(cmd, check=True)
    verr = gsolve_nleqns(gfun_solver,
            method=rmethod,
            jac=jac,
            args={
                    "mpi":mpi,
                    "path":path,
                    "iembeddiag":iembeddiag,
                    "edinit":edinit,
                    "cygtail":cygtail,
                    },
            tol=tol,
            )
    print(f" gerr = {np.max(np.abs(verr))}.")
    cmd = mpi + [f"{path}/CyGFinal{cygtail}", "-r", "0"]
    print(" ".join(cmd))
    subprocess.run(cmd, check=True)


def check_jacobian():
    args = get_inline_args()
    # get the point around which the jacobian is calculated.
    with h5py.File("GIter.h5", "r") as f:
        x0 = f["/x_val"][()]
    args_ = {
            "mpi":args.mpi,
            "path":args.path,
            "iembeddiag":args.iembeddiag,
            "edinit":args.edinit,
            "cygtail":get_cygtail(),
            }
    jac_num = get_jacobian_numerical(
            x0.copy(),
            args=args_,
            delta=1.e-5,
            )
    print(" Jacobian-numeric:")
    for row in jac_num:
        print("".join(f"{a:8.4f} " for a in row))

    jac_ana = get_jacobian_analytical(
            x0.copy(),
            args=args_,
            )
    print(" Jacobian-analytical:")
    for row in jac_ana:
        print("".join(f"{a:8.4f} " for a in row))

    diff = jac_num - jac_ana
    print(" Jacobian difference:")
    for row in diff:
        print("".join(f"{a:8.4f} " for a in row))
    print(f" max diff = {np.max(np.abs(diff)):8.4f}")


def check_jac_ndlc(dsetnames1=["/d_coef", "/lc_coef"],
        dsetnames2=["/PD_COEFPR", "/PD_COEFPL",
                "/PLC_COEFPR", "/PLC_COEFPL"]):
    args = get_inline_args()
    # get the point around which the jacobian is calculated.
    with h5py.File("GIter.h5", "r") as f:
        x0 = f["/x_val"][()]
    args_ = {
            "mpi":args.mpi,
            "path":args.path,
            "iembeddiag":args.iembeddiag,
            "cygtail":get_cygtail(),
            "dsetname1":dsetnames1[0],
            "dsetname2":dsetnames1[1],
            }
    jac_num = get_jacobian_numerical(
            x0.copy(),
            args=args_,
            vec_fun=ndlc_fun,
            delta=1.e-4,
            )
    print(" Jacobian-numeric:")
    print(f" max: {np.max(jac_num):8.2e}, min {np.min(jac_num):8.2e}")
    for row in jac_num:
        print("".join(f"{a:8.4f} " for a in row))

    # analytical jacobian
    exec_cygband(x0, **args_, jac=True)
    with h5py.File("GDLDeri.h5", "r") as f:
        dset1 = f[dsetnames2[0]][()].T
        dset2 = f[dsetnames2[1]][()].T
        dset3 = f[dsetnames2[2]][()].T
        dset4 = f[dsetnames2[3]][()].T
    pd = np.concatenate((dset1, dset2), axis=1)
    plc = np.concatenate((dset3, dset4), axis=1)
    jac_ana = np.concatenate((pd, plc))
    print(" Jacobian-analytical:")
    print(f" max: {np.max(jac_ana):8.2e}, min {np.min(jac_ana):8.2e}")
    for row in jac_ana:
        print("".join(f"{a:8.4f} " for a in row))
    diff = jac_num - jac_ana
    print(" Jacobian difference:")
    print(f" max: {np.max(diff):8.2e}, min {np.min(diff):8.2e}")
    for row in diff:
        print("".join(f"{a:8.4f} " for a in row))


def check_jac_dlc():
    check_jac_ndlc()


def check_jac_dm():
    check_jac_ndlc(
            dsetnames1=["/d0_coef", "/nks_coef"],
            dsetnames2=["/PD0_COEFPR", "/PD0_COEFPL",
                    "/PDM_COEFPR", "/PDM_COEFPL"])
