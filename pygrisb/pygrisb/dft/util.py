import glob, sys, os


def get_dir_list(key="V", chk_link=True):
    if "-dkey" in sys.argv:
        key = sys.argv[sys.argv.index("-dkey")+1]
    dir_list = glob.glob(f'{key}*')
    v_list = []
    for i, v in enumerate(dir_list):
        if "_" not in key and "_" in v:
            continue
        if chk_link and os.path.islink(v):
            continue
        v_list.append(int(v.split(key)[1]))
    v_list.sort()
    dir_list = [f'{key}{v}' for v in v_list]
    print(f"work in dirs {dir_list}.")
    ans = input("proceed? [Y/n]")
    if "n" in ans.lower():
        sys.exit()
    return dir_list

