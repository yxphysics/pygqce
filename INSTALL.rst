Install
=======
* Easiest way via Anaconda::

    conda create -n pygrisb-env python=3 pygrisb -c yxphysics -c matsci

  This creates an independent Conda environment for *pygrisb*. 
  Be sure to execute::

    conda activate pygrisb-env

  in order to use *pygrisb*. 

* To install a local version with source files, just type::

    make 

  It will establish a link to the source directory.

Troubleshooting
================
* Error associated with importing *pymatgen*.
  
  There are chances for lib mismatch. The possible way out is the following 
  (within pygrisb-env)::

    conda uninstall pymatgen 
    # it uninstalls pygrisb automatically due to dependencies.
    pip install pymatgen 
    conda install pygrisb -c yxphysics # now reinstall pygrisb

* Error associated with *mpi4py*.

  There are chances for lib mismatch. The possible way out is the following 
  (within pygrisb-env)::

    conda uninstall mpi4py 
    # it uninstalls pygrisb automatically due to dependencies.
    # set your environment variable MPICC
    export MPICC=your_mpi_c_compiler
    pip install mpi4py
    conda install pygrisb -c yxphysics # now reinstall pygrisb

Please kindly report issues to https://gitlab.com/yxphysics/pygrisb/issues.
