# pygqce

Adapted version of "pygrisb" package for Gutzwiller quantum-classical embedding (GQCE) simulation framework. "CyGutz" package (https://gitlab.com/yxphysics/cygutz) and modified version of qucochemistry (https://github.com/yaoyongxin/qucochemistry/tree/master/qucochemistry) need to be installed as well for calculations on model systems. 

"pygrisb" is a python library for preprocessing, managing and postprocessing the (DFT+) Gutzwiller-RISB calculations. It is used in [comsuite](https://www.bnl.gov/comscope/software/comsuite.php), developed in US DOE's Center for Computational Material Spectroscopy and Design at Brookhaven National Laboratory.
